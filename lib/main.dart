import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';


void main() {
  runApp(const ExampleApp());
}

class ExampleApp extends StatelessWidget {
  const ExampleApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: Scaffold(
          appBar: buildAppBarWidget(),
          body: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(32),
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("img/im2.jpg"), fit: BoxFit.cover),
            ),
            child: Column(
              children: [
                Text(
                  "ชลบุรี",
                  style: TextStyle(fontSize: 40,color: Colors.white),
                ),
                Icon(Icons.location_on,size: 28,color: Colors.white),
                Text(
                  "25°C",
                  style: TextStyle(fontSize: 60,color: Colors.white),
                ),
                Text(
                  "24°C/27°C",
                  style: TextStyle(fontSize: 20,color: Colors.white),
                ),
                Text(
                  "แดดจัดบางส่วน",
                  style: TextStyle(fontSize: 20,color: Colors.white),
                ),
                const SizedBox(
                  height: 50,
                ),
                Container(
                  height: 120,
                  decoration: BoxDecoration(
                      color: Colors.black38,
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                          offset: const Offset(0, 25),
                          blurRadius: 18,
                          spreadRadius: -12,
                        )
                      ]),
                  child: weatherActionItems(),
                ),
                const SizedBox(
                  height: 50,
                ),
                Container(
                  height: 150,
                  decoration: BoxDecoration(
                      color: Colors.black38,
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                          offset: const Offset(0, 25),
                          blurRadius: 18,
                          spreadRadius: -12,
                        )
                      ]),
                  child: weatherActionItems2(),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.blueGrey,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),
    // title: Text("Hello Flutter App"),
    actions: <Widget>[
      IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.search,
            color: Colors.black,
          )),
    ],
  );
}

Widget weatherActionItems(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      weather1Button(),
      weather2Button(),
      weather3Button(),
      weather4Button(),

    ],
  );
}

Widget weatherActionItems2(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      weather11Button(),
      weather22Button(),
      weather33Button(),
      weather44Button(),

    ],
  );
}



Widget weather1Button() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text("12:00",style: TextStyle(fontSize: 18,color: Colors.white)),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("23°C",style: TextStyle(fontSize: 18,color: Colors.white)),
    ],
  );
}

Widget weather2Button() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text("13:00",style: TextStyle(fontSize: 18,color: Colors.white)),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.orange,
        ),
        onPressed: () {},
      ),
      Text("26°C",style: TextStyle(fontSize: 18,color: Colors.white)),
    ],
  );
}
Widget weather3Button() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text("14:00",style: TextStyle(fontSize: 18,color: Colors.white)),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("24°C",style: TextStyle(fontSize: 18,color: Colors.white)),
    ],
  );
}

Widget weather4Button() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text("15:00",style: TextStyle(fontSize: 18,color: Colors.white)),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("20°C",style: TextStyle(fontSize: 18,color: Colors.white)),
    ],
  );
}

Widget weather11Button() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text("วันพรุ่งนี้",style: TextStyle(fontSize: 18,color: Colors.white)),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.orange,
        ),
        onPressed: () {},
      ),
      Text("28°C",style: TextStyle(fontSize: 18,color: Colors.white)),
      Text("มีแดดจัด",style: TextStyle(fontSize: 14,color: Colors.white)),
    ],
  );
}

Widget weather22Button() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text("อ.",style: TextStyle(fontSize: 18,color: Colors.white)),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.orange,
        ),
        onPressed: () {},
      ),
      Text("29°C",style: TextStyle(fontSize: 18,color: Colors.white)),
      Text("มีแดดจัด",style: TextStyle(fontSize: 14,color: Colors.white)),
    ],
  );
}

Widget weather33Button() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text("พ.",style: TextStyle(fontSize: 18,color: Colors.white)),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("22°C",style: TextStyle(fontSize: 18,color: Colors.white)),
      Text("มีฝนปรอยๆ",style: TextStyle(fontSize: 14,color: Colors.white)),
    ],
  );
}

Widget weather44Button() {
  return Column(
    children: <Widget>[
      const SizedBox(
        height: 10,
      ),
      Text("พฤ.",style: TextStyle(fontSize: 18,color: Colors.white)),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("25°C",style: TextStyle(fontSize: 18,color: Colors.white)),
      Text("มีเมฆมาก",style: TextStyle(fontSize: 14,color: Colors.white)),
    ],
  );
}